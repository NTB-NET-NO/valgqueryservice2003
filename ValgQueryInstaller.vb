Imports System.ComponentModel
Imports System.Configuration.Install

<System.ComponentModel.RunInstallerAttribute(True)> Public Class ValgQueryInstaller
    Inherits System.Configuration.Install.Installer

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Installer overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ValgServiceInstaller As System.ServiceProcess.ServiceInstaller
    Friend WithEvents ValgProcessInstaller As System.ServiceProcess.ServiceProcessInstaller
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ValgProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.ValgServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'ValgProcessInstaller
        '
        Me.ValgProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.ValgProcessInstaller.Password = Nothing
        Me.ValgProcessInstaller.Username = Nothing
        '
        'ValgServiceInstaller
        '
        Me.ValgServiceInstaller.DisplayName = "ValgQuery"
        Me.ValgServiceInstaller.ServiceName = "ValgQueryService"
        Me.ValgServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ValgQueryInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.ValgProcessInstaller, Me.ValgServiceInstaller})

    End Sub

#End Region

End Class
